# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @Time    : 2021/8/23 0023 7:49
# @Function:
"""
参数化两种方法：
方法一：@pytest.mark.parametrize 读取参数
方法二：@pytest.fixture 读取参数
"""
import logging

import allure
import pytest
from homework_one1.testing1.test_yml import get_datas_add, get_datas_div

url = "http://www.baidu.com"
@allure.testcase(url,"测试用例链接")
@allure.feature("数据加法与除法计算")
class TestCalcu:
    @allure.title("简单相加：")
    @allure.story("简单相加")
    @pytest.mark.run(order=2)
    def test_simple_add(self,get_calculator):
        print("第二个执行的")
        res1 =get_calculator.add(2,4)
        assert res1 == 6

    @allure.title("简单相除")
    @pytest.mark.run(order=4)
    def test_simple_div(self,get_calculator):
        res1 = get_calculator.div(6,2)
        assert res1 == 3


    # 加法重复，markparametrize
    @allure.title("参数化方法：@pytes.makr.parametrize")
    @pytest.mark.parametrize('a,b,expect', get_datas_add()[0], ids=get_datas_add()[1])
    def test_add1(self, a, b, expect, get_calculator):
        res1 = get_calculator.add(a, b)
        assert res1 == expect


    @pytest.fixture(params=get_datas_add()[0], ids=get_datas_add()[1])
    def get_byfixture_add(self, request):
        with allure.step("加法参数化第一步："):
            print(f"打印参数数据：{get_datas_add()}")

            return request.param

    @allure.title("@pest.fixture参数化：参数相加")
    @pytest.mark.run(order=1)
    def test_fixture_add(self,get_calculator,get_byfixture_add):
        with allure.step("参数化加法第二步："):
            print("输入打印数据")
        result = get_calculator.add(get_byfixture_add[0], get_byfixture_add[1])

        assert result == get_byfixture_add[2]

    @pytest.fixture(params=get_datas_div()[0], ids=get_datas_div()[1])
    def get_byfixture_div(self, request):
        return request.param


    @allure.story("参数相除")
    @pytest.mark.run(order=3)
    def test_fixture_div(self, get_calculator, get_byfixture_div):
        # logging.info(f"测试相除功能日志，参数{str(get_byfixture_div)}")

        logging.info(f"测试相除功能日志，参数{str(get_byfixture_div)}")
        result = get_calculator.div(get_byfixture_div[0], get_byfixture_div[1])
        assert result == get_byfixture_div[2]

    @allure.story("截图")
    def test_aa(self):
        allure.attach.file("D:/QMDownload/风景图.jpeg",name="这是一个手势图片",attachment_type=allure.attachment_type.JPG)
        print("hello world")

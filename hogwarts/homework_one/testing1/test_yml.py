# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @Time    : 2021/8/23 0023 23:14
# @Function:
#读取datas文件夹下的data.yml数据
import yaml

def get_datas_add():
    with open('../../datas/data.yml', encoding='utf-8') as f:
        datas = yaml.safe_load(f)
        add_datas = datas.get("add").get("int").get("datas")
        add_ids = datas.get("add").get("int").get("ids")
        return (add_datas,add_ids)

def get_datas_div():
    with open('../../datas/data.yml', encoding='utf-8') as f:
        datas = yaml.safe_load(f)
        div_datas = datas.get("div").get("int").get("datas")
        div_ids = datas.get("div").get("int").get("ids")
        return (div_datas,div_ids)

def test_cs():
    print(get_datas_add())
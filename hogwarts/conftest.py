# !/usr/bin/env python
# -*- coding:UTF-8 -*-
# @Author  : tonywang
# @Time    : 2021/8/23 0023 7:43
# @Function:
import sys
import time

#系统找不到路径会报错，所以添加sys.path.append处理
sys.path.append('../../')

import pytest
from homework_one.calculator.calculator import Calculator


@pytest.fixture(scope="session")
def get_calculator():
    calcu = Calculator
    print("计算开始")
    yield calcu
    print("计算结束")

@pytest.fixture(autouse=True)
def login():
    print("登录成功")

# 日志文件安时间命名自动生成
@pytest.fixture(scope="session", autouse=True)
def manage_logs(request):
    """Set log file name same as test name"""
    now = time.strftime("%Y-%m-%d %H-%M-%S")
    rootdir= request.config.rootdir
    print(rootdir)

    log_name = rootdir + '/output/log_' + now + '.logs'

    request.config.pluginmanager.get_plugin("logging-plugin") \
        .set_log_path(log_name)
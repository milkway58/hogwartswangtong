# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @file    : contact_page.py
# @Time    : 2021/9/7 11:26
# @Function:
from selenium.webdriver.common.by import By

from pageobject.base import BasePage


class ContactPage(BasePage):

    def get_members(self):

        # 通讯录列表
        eles = self.driver.find_elements(By.CSS_SELECTOR, ".member_colRight_memberTable_td:nth-child(5)")
        phone_list = []
        for ele in eles:
            phone_list.append(ele.text)
        # print(phone_list)
        return phone_list





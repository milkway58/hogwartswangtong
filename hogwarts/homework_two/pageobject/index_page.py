# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @file    : index_page.py
# @Time    : 2021/9/7 11:25
# @Function:
from time import sleep

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


from pageobject.add_memeber import AddMember
from pageobject.base import BasePage
from pageobject.contact_page import ContactPage


class IndexPage(BasePage):
    _base_url = "https://work.weixin.qq.com/wework_admin/frame#index"
    _css_div = (By.CSS_SELECTOR,".index_service_cnt_item span")
    #首页跳转添加成员页面
    def goto_contact(self):
        return  ContactPage()


    def goto_add_member(self):
        # # WebDriverWait(self.driver,10).until(expected_conditions.element_to_be_clickable(By.CSS_SELECTOR,"div > span.ww_indexImg.ww_indexImg_AddMember"))
        # # self.driver.implicitly_wait(10)
        # WebDriverWait(self.driver,20).until(expected_conditions.element_to_be_clickable(self._css_div))
        # self.driver.find_element(By.CSS_SELECTOR,"div > span.ww_indexImg.ww_indexImg_AddMember").click()
        sleep(5)
        self.find(self._css_div).click()




        return AddMember(self.driver)

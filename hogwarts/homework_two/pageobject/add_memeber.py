# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @file    : add_memeber.py
# @Time    : 2021/9/7 11:26
# @Function:
from selenium.webdriver.common.by import By

from pageobject.base import BasePage
from pageobject.contact_page import ContactPage


class AddMember(BasePage):

    #私有化封装元素定位
    _CSS_user = (By.CSS_SELECTOR, "#username")
    _CSS_member = (By.CSS_SELECTOR, "#memberAdd_acctid")
    _CSS_member_phone = (By.CSS_SELECTOR, "#memberAdd_phone")
    _TEXT = (By.LINK_TEXT, "保存")



    #添加成员页面跳转到通信录页面
    def goto_contact(self):

        return ContactPage()
    #不相干的用例内容去testcase去添加，页面只保存逻辑
    def add_member(self,name,phone,number):
        self.driver.implicitly_wait(10)
        self.find(self._CSS_user).send_keys(name)
        self.find(self._CSS_member).send_keys(number)
        self.find(self._CSS_member_phone).send_keys(phone)
        self.find(self._TEXT).click()


        return   ContactPage(self.driver)

    # def add_member_fail(self, name,phone,number):
    #     self.driver.implicitly_wait(10)
    #
    #     self.find(self._CSS_user).send_keys(name)
    #     self.find(self._CSS_member).send_keys(number)
    #     self.find(self._CSS_member_phone).send_keys(phone)
    #     self.find(self._TEXT).click()
    #     erro_list = self.find(By.CSS_SELECTOR,".ww_inputWithTips_WithErr div")
    #     erro_list = []
    #     for erro in erro_list:
    #         erro_list.append(erro.text)
    #         print(erro_list)
    #
    #     return erro_list


# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @file    : base.py
# @Time    : 2021/9/7 11:25
# @Function:
from time import sleep

import yaml
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver


class BasePage:

    #封装其他页面共同的方法
    #必须要导入WebDriver,不然后面无法调用selenium浏览器
    def __init__(self,base_driver:WebDriver=None):

        if base_driver == None:

            self.driver = webdriver.Chrome()
            self.driver.maximize_window()
            self.driver.get(self._base_url)
            cookie = yaml.safe_load(open("../conf/cookie.yaml", "r"))

            for c in cookie:
                self.driver.add_cookie(c)
            sleep(3)
            self.driver.get(self._base_url)

        else:
            self.driver = base_driver
            self.driver.maximize_window()

    # # 查询元素方法封装
    # def find(self,by,locator):
    #     """
    #     :param by: 定位方式
    #     :param locator: 定位元素
    #     :return: 返回值
    #     """
    #     return self.driver.find_element(by=by,value=locator)
    def find(self, by, locator=None):
        if locator == None:
            # 如果 locator为none,那么认为传入的数据就是一个元祖
            # 元解包(1,2), *(1,2), 分别以1， 2 形式的参数传入
            web_ele = self.driver.find_element(*by)
        else:
            web_ele = self.driver.find_element(by=by, value=locator)
        # print(f"查找到的元素为{web_ele}")
        return web_ele

    def setup(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
        self.driver.maximize_window()
        print("这个是一个方法1")
    def teardown(self):
        self.driver.quit()
        self.driver.close()


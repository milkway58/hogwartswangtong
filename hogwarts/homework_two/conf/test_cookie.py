# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @file    : test_cookie.py
# @Time    : 2021/9/7 10:16
# @Function:
import yaml
from selenium import webdriver
from time import sleep

class TestCookie:

    def setup_class(self):
        self.driver = webdriver.Chrome()
        # print(f"第一次打印查看self.driver{self.driver}")


    #获取cookie并保存到cookie.yaml文件中
    def test_getcookie(self):
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome_baidu")

        sleep(20)
        cookie = self.driver.get_cookies()
        print(cookie)
        with open("./cookie.yaml","w")as f:
            yaml.safe_dump(cookie,f)
    #读取cookie.yaml文件，并利用cookie文件进行登录
    # def test_getcookie(),扫码一次后不要再调用，不然test_add_cookie获取cookie更新是无法登录的
    def test_add_cookie(self):
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome_baidu")
        self.driver.maximize_window()
        #读取yaml文件并转换为python格式cookie
        cookie = yaml.safe_load(open("./cookie.yaml","r"))

        for c in cookie:
            self.driver.add_cookie(c)
        sleep(3)
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome_baidu")


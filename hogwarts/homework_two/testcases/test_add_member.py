# !/usr/bin/env python
# -*- coding:utf-8 -*-
# @Author  : tonywang
# @file    : test_add_member.py
# @Time    : 2021/9/7 11:27
# @Function:

import pytest

from pageobject.index_page import IndexPage

index1 = IndexPage()
class TestAddMember:

    datas = [("小花花1","13222222221","111"),("小花花2","13222222222","112"),("huahua3","13222222223","113")]
    @pytest.mark.parametrize(("a,b,c"),datas)
    def test_add(self,a,b,c):

        phone_list = index1.goto_add_member().add_member(a,b,c).get_members()

        assert  "13998023628" in phone_list

    # def test_fail(self):
    #
    #     error_message = index1.goto_add_member().add_member_fail("huahua3","13222222223","113")
    #     assert "huahua3" in error_message
